# VimiumMaterialDesign

A [Material Design](https://material.io) CSS file for the [Vimium browser plug-in](https://vimium.github.io)'s UI

![Imgur](https://i.imgur.com/ihsdOHU.png)

## Installation

Copy the content of [linkhints.css](linkhints.css) into the *CSS for Vimium UI* text box in the Vimium options.
